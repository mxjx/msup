<?php

/**
 * @package application.modules.template 
 */
class Mail_listModule extends CWebModule {
	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'mail_list.models.*',
			'mail_list.components.*',
			// 'application.controllers.*',
			'application.components.*',
		));
	}
}
