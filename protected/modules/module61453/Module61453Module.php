<?php

/**
 * @package application.modules.template 
 */
class Module61453Module extends CWebModule {
	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'module61453.models.*',
			'module61453.components.*',
			// 'application.controllers.*',
			'application.components.*',
		));
	}
}
