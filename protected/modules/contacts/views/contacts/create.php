<?php
/*****************************************************************************************
 * X2Engine Open Source Edition is a customer relationship management program developed by
 * X2Engine, Inc. Copyright (C) 2011-2014 X2Engine Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY X2ENGINE, X2ENGINE DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact X2Engine, Inc. P.O. Box 66752, Scotts Valley,
 * California 95067, USA. or at email address contact@x2engine.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * X2Engine" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by X2Engine".
 *****************************************************************************************/

$menuItems = array(
	array('label'=>Yii::t('contacts','All Contacts'),'url'=>array('index')),
	array('label'=>Yii::t('contacts','Lists'),'url'=>array('lists')),
	array('label'=>Yii::t('contacts','Create Contact')),
);

$opportunityModule = Modules::model()->findByAttributes(array('name'=>'opportunities'));
$accountModule = Modules::model()->findByAttributes(array('name'=>'accounts'));

if($opportunityModule->visible && $accountModule->visible)
	$menuItems[] = 	array('label'=>Yii::t('app', 'Quick Create'), 'url'=>array('/site/createRecords', 'ret'=>'contacts'), 'linkOptions'=>array('id'=>'x2-create-multiple-records-button', 'class'=>'x2-hint', 'title'=>Yii::t('app', 'Create a Contact, Account, and Opportunity.')));

$this->actionMenu = $this->formatMenu($menuItems);

?>

<div class="page-title icon contacts">
	<h2><?php echo Yii::t('contacts','Create Contact'); ?></h2>
</div>
<!-- contactsInfo -->
<div id="modelInfo">
    

    <?php 

    echo $this->renderPartial(
        'application.components.views._form', 
        array(
            'model'=>new Contacts(),
            'users'=>$users,
            'modelName'=>'contacts',
            'defaultsByRelatedModelType' => array (
                'Accounts' => array (
                    'phone' => 'js: $("div.formInputBox #Contacts_phone").val();',
                    'website' => 'js: $("div.formInputBox #Contacts_website").val();',
                    'assignedTo' => 'js: $("#Contacts_assignedTo_assignedToDropdown").val();'
                )
            )
        )); 

    if(isset($_POST['x2ajax'])) {
        echo "<script>\n";
        Yii::app()->clientScript->echoScripts();
        echo "\n</script>";
    }

    ?>
    <input type="hidden" name="test" value="555">
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->getBaseUrl();?>/css/contactsForm.css">
<!-- moreInfo Begin -->
<div id="moreInfo">
    

    <!-- 附属表中的基本信息 -->
    <div id="baseInfo">
        <!-- 通讯录 Begin -->
        <?php 

        echo $this->renderPartial(
            'application.components.views._form', 
            array(
                'model'=>new Mail_list(),
                'users'=>$users,
                'modelName'=>'mail_list',
            )); 

        ?>
        <!-- 通讯录 End -->

        <!-- 地址 Begin -->
        <?php 

        echo $this->renderPartial(
            'application.components.views._form', 
            array(
                'model'=>new Address(),
                'users'=>$users,
                'modelName'=>'address',
            )); 

        ?>
        <!-- 地址 End -->
    </div>
    <!-- 附属表其他信息 -->
    <div id="detail">
        
    </div>

</div>
<script type="text/javascript">
    $(function(){
        $("#moreInfo").insertAfter($("#modelInfo .formSection").eq(0));
        $("#baseInfo em").remove();
        $("#baseInfo  form .row.buttons .x2-button").remove();
        $("#baseInfo  form .row.buttons").append('<input class=" addOne x2-button"  tabindex="24" type="button" name="yt2" name="addOne" value="新增一行">');

        $("#baseInfo form").each(function(){
            $(this).append("<div class='copyInput'></div>");
            $(this).find('.tableWrapper').clone().appendTo($(this).find(".copyInput"));
        })
        $(".addOne").click(function(event) {
            var parent =  $(this).parents("form")
            var thisForm = parent[0];
            var cloneInput = $(thisForm).find(".copyInput").find(".tableWrapper").clone();
            $(cloneInput).appendTo($(thisForm).find(".formSection"));
        });
    
    $("form").submit(function(){

        // $.post('/contacts/create', 
        //     Contacts: 'value1', 
        //     function(data, textStatus, xhr) {
        //     /*optional stuff to do after success */
        // });
        console.log($("#contacts-form").serialize());
        return false;
        $("#moreInfo form").each(function(){
            $(this).find("input").each(function(e){

                $(this).attr("name", "Contacts[moreInfo]["+$(this).attr("name")+"]");
            })
        
        })
        
    })

    })
</script>
<!-- moreInfo End -->